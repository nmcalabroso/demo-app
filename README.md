# Demo App

## A [Ruby on Rails](http://www.rubyonrails.org) sample application

This is the sample application for
the [Ruby on Rails Tutorial](http://railstutorial.org/)
by [Michael Hartl](http://michaelhartl.com/).