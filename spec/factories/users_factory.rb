FactoryGirl.define do
  factory :user do
    name                  "nmcalabroso"
    email                 "nmcalabroso@example.com"
    password              "password"
    password_confirmation "password"
  end
end
