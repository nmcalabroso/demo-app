require 'capybara/rspec'
require 'database_cleaner'

RSpec.configure do |config|
  config.include Capybara::DSL
end

Capybara.configure do |config|
  config.run_server = true
  config.server_port = 7000
  config.default_driver = :selenium
  config.app_host = "localhost:#{config.server_port}"
end
