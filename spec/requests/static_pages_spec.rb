require 'rails_helper'

describe "Static pages", :type => :feature do

  let(:site_name) { "DemoApp" }

  it "should have the correct links in the navbar" do
    visit root_path
    click_link "Home"
    expect(page).to have_title "#{site_name} | Home"
    click_link "Help"
    expect(page).to have_title "#{site_name} | Help"
    click_link "About"
    expect(page).to have_title "#{site_name} | About"
    click_link "Contact"
    expect(page).to have_title "#{site_name} | Contact"
  end

  describe "Home page" do

    before do
      visit home_path
    end

    it "should have the title 'Home'" do
      expect(page).to have_title "#{site_name} | Home"
    end

    it "should have the content 'Demo App'" do
      expect(page).to have_content 'Demo App'
    end

    it "should have the correct 'sign-up' link" do
      click_link "Sign up now!"
      expect(page).to have_title "#{site_name} | Sign Up"
    end
  end

  describe "Help page" do

    before do
      visit help_path
    end

    it "should have the title 'Help'" do
      expect(page).to have_title "#{site_name} | Help"
    end

    it "should have the content 'Help'" do
      expect(page).to have_content 'Help'
    end
  end

  describe "About page" do

    before do
      visit about_path
    end

    it "should have the title 'About'" do
      expect(page).to have_title "#{site_name} | About"
    end

    it "should have the content 'About Us'" do
      expect(page).to have_content 'About Us'
    end
  end

  describe "Contact page" do

    before do
      visit contact_path
    end

    it "should have the title 'Contact" do
      expect(page).to have_title "#{site_name} | Contact"
    end

    it "should have the content 'Contact Us'" do
      expect(page).to have_content 'Contact Us'
    end
  end
end
