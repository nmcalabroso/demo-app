require 'rails_helper'

describe "User pages" do
  let(:site_name) { "DemoApp" }

  subject { page }

  describe "sign-up page" do
    before { visit signup_path }

    it { should have_content "Sign up" }
    it { should have_title "#{site_name} | Sign Up" }
  end

  describe "signup" do
    before { visit signup_path }
    let(:submit) { "Create new account" }

    describe "with valid information" do

      before do
        fill_in "Name",         with: "ExampleUser"
        fill_in "Email",        with: "sample_user@example.com"
        fill_in "Password",     with: "password"
        fill_in "Confirmation", with: "password"
        click_button submit
      end

      let(:user) { User.find_by(email: 'sample_user@example.com') }

      it "should create a user and redirect to user's profile page" do
        expect(page).to have_link 'Sign out'
        expect(page).to have_title user.name
        expect(page).to have_selector 'div.alert.alert-success', text: 'Welcome'
      end
    end

    describe "with invalid information" do

      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end
    end
  end

  describe "profile page" do
    let(:user) { FactoryGirl.create(:user) }
    before { visit user_path(user) }
    it { should have_content "#{site_name} | #{user.name}" }
    it { should have_title user.name }
  end

  describe "edit" do
    let(:user) { FactoryGirl.create(:user) }
    before { visit edit_user_path(user) }

    describe "page" do
      it { should have_content "Update your profile" }
      it { should have_title "Edit user" }
      it { should have_link "change", href: 'http://gravatar.com/emails' }
    end

    describe "with invalid information" do
      before { click_button "Save changes" }

      it { should have_content "error" }
    end
  end
end
